(function ($) {
    'use strict';

    var App = {};

    App.MobileMenu = {
        handle: $('#mobile-toggle'),
        nav: $('.nav-mobile'),

        toggleMenu: function(event) {
            event.preventDefault();
            this.nav.slideToggle();
            this.handle.toggleClass('close')
        },

        init: function() {
            this.handle.on('click touchstart', this.toggleMenu.bind(this));
        }
    };

    App.Home = {
        bannersWrapper: $('.banners'),
        depoimentosWrapper: $('.depoimentos'),

        cycleConfig: {
            slides: '>.slide',
            pager: '>.center .cycle-pager',
            pagerTemplate: '<a href="#">{{slideNum}}</a>',
            fx: 'scrollHorz',
            autoHeight: 'calc'
        },

        init: function() {
            this.bannersWrapper.cycle(this.cycleConfig);
            this.depoimentosWrapper.cycle(this.cycleConfig);
        }
    };

    App.Accordion = {
        handle: $('.accordion-handle'),

        handleToggle: function(event) {
            event.preventDefault();

            var target = $(event.target);
            if (target.hasClass('active')) return this.close(target);

            this.closeAll();
            this.open(target);
        },

        open: function(elem) {
            elem.addClass('active');
            elem.next().slideDown();
        },

        close: function(elem) {
            elem.removeClass('active');
            elem.next().slideUp();
        },

        closeAll: function() {
            var _this = this;
            this.handle.each(function(index, elem) {
                _this.close($(elem));
            });
        },

        init: function() {
            this.handle.on('click', this.handleToggle.bind(this));
        }
    };

    App.Galeria = {
        handle: $('.fancybox'),

        fancyboxConfig: {
            padding: 10,
            helpers: {
                overlay: { locked: false }
            }
        },

        init: function() {
            this.handle.fancybox(this.fancyboxConfig);
        }
    };

    App.InformacoesConsultores = {
        handle: $('.mais-informacoes-handle'),

        handleToggle: function(event) {
            event.preventDefault();
            var handle = $(this);

            handle.prev().slideToggle(function() {
                handle.toggleClass('active');
            });
        },

        init: function() {
            this.handle.on('click', this.handleToggle);
        }
    };

    App.FormularioCEP = {
        handle: $('.cep-handle'),

        validaCep: function(cep) {
            cep = cep.replace(/\D/g, '');
            if (/^[0-9]{8}$/.test(cep)) {
                this.carregaEndereco(cep);
            }
        },

        carregaEndereco: function(cep) {
            var self = this;

            $(this.currentEl).next('.loading').fadeIn('fast', function () {
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/", function(dados) {
                    setTimeout(function() {
                        self.mostraDados(dados)
                    }, 100);
                });
            });
        },

        mostraDados: function(dados) {
            $(this.currentEl).next('.loading').fadeOut('fast', function() {
                var campos = $(this).parent().next('.campos-cep');

                campos.find('.handle-endereco').val(dados.logradouro || '');
                campos.find('.handle-bairro').val(dados.bairro || '');
                campos.find('.handle-cidade').val(dados.localidade || '');
                campos.find('.handle-uf').val(dados.uf || '');

                campos.removeClass('hidden');
            });
        },

        init: function() {
            var timer, self = this;

            this.handle.inputmask('99999-999');
            this.handle.each(function(index, el) {
                el = $(el);
                el.on('input', function() {
                    if (timer) clearTimeout(timer);
                    timer = setTimeout(function() {
                        self.currentEl = el;
                        self.validaCep(el.val());
                    }, 1000);
                });
            });
        }
    };

    App.AdicionaParticipante = {
        handle: $('.adicionar-participante'),

        inputTemplate: '<div class="participante" style="display:none"><label><span>nome completo do participante</span><input type="text" name="nome[]"></label><label><span>e-mail do participante</span><input type="email" name="email[]"></label></div>',

        adicionaInput: function(event) {
            event.preventDefault();
            $(this.inputTemplate).insertBefore(this.handle).fadeIn();
        },

        init: function() {
            this.handle.on('click', this.adicionaInput.bind(this));
        }
    };

    App.FormularioPagamento = {
        handle: $('input[type=radio][name=responsavel_pagamento]'),

        toggleForms: function() {
            var formClass;

            $('.form-hidden').slideUp();

            App.EnderecoCobranca.resetState();

            if (this.value !== 'mesmo') {
                formClass = (this.value == 'pessoa_fisica') ? '.form-pessoa-fisica' : '.form-pessoa-juridica';
                $(formClass).slideDown();
            }
        },

        init: function() {
            this.handle.on('change', this.toggleForms);
        }
    };

    App.EnderecoCobranca = {
        handle: $('.endereco-cobranca-radio'),
        form: $('.endereco-novo-form'),

        toggleForms: function(event) {
            if (event.target.value === 'novo') {
                this.form.slideDown();
            } else {
                this.form.slideUp();
            }
        },

        init: function() {
            this.handle.on('change', this.toggleForms.bind(this));
        },

        resetState: function() {
            this.handle.each(function(index,el) {
                el = $(el);
                if (el.val() === 'mesmo') el.prop('checked', true);
            });
            this.form.hide();
            this.form.find('.cep-handle').val('');
            this.form.find('.campos-cep').addClass('hidden').find('input').val('');
        }
    };

    $(document).ready(function() {
        App.MobileMenu.init();
        App.Home.init();
        App.Accordion.init();
        App.Galeria.init();
        App.InformacoesConsultores.init();
        App.FormularioCEP.init();
        App.AdicionaParticipante.init();
        App.FormularioPagamento.init();
        App.EnderecoCobranca.init();
    });

})(jQuery);