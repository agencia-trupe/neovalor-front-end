<?php
    $url = 'http://neovalor.dev/';
?>
<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="2015 Trupe Agência Criativa">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:site_name" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <title>neovalor &middot; treinamento e consultoria</title>

    <link rel="stylesheet" href="<?=$url?>assets/vendor/fancybox/source/jquery.fancybox.css">
    <link rel="stylesheet" href="<?=$url?>assets/css/main.min.css">
</head>
<body>

<?php

$page = (isset($_GET['view']) ? $_GET['view'] : 'home');
$menu = explode('/', $page)[0];

if (is_file('views/'.$page.'.php')) {
    $view = $page;
} elseif (is_file('views/'.$page.'/index.php')) {
    $view = $page.'/index';
} else {
    $view = 'common/404';
    $menu = '';
}

function isHome($menu) {
    return preg_match('/^home.*/', $menu);
}

include 'views/common/header.php';
include 'views/'.$view.'.php';
include 'views/common/footer.php';

?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?=$url?>assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>
    <script src="<?=$url?>assets/vendor/jquery-cycle2/build/jquery.cycle2.min.js"></script>
    <script src="<?=$url?>assets/vendor/fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="<?=$url?>assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="<?=$url?>assets/js/main.js"></script>
</body>
</html>
