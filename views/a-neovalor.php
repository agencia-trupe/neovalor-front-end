<div class="content a-neovalor">
    <div class="center">
        <div class="content-left">
            <img src="<?=$url?>assets/img/img-aneovalor.png" alt="">
        </div>

        <div class="content-main">
            <h1>A NEOVALOR</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea tempora quasi   assumenda consequatur illo optio, alias maxime, dignissimos repellendus beatae quas nihil. Aliquam, consequuntur enim!</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem voluptate, numquam tenetur impedit saepe, totam sunt neque rerum unde nostrum consequatur nulla consectetur recusandae nemo?</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro quibusdam voluptatum, voluptas ullam aspernatur quasi vitae suscipit quidem impedit magnam doloremque non aut vel, nihil!</p>

            <div class="objetivo-estrategico">
                <h2>OBJETIVO ESTRATÉGICO</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum quod aliquam, temporibus odio eaque harum iusto. Quaerat ratione, impedit, provident cupiditate reiciendis repudiandae reprehenderit cum.</p>
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                </ul>
            </div>

            <a href="<?=$url?>contato" class="conversa">
                <span>CONVIDAMOS VOCÊ<br> PARA UMA CONVERSA</span>
                <img src="<?=$url?>assets/img/img-convidamosvoce-neovalor.png" alt="">
            </a>
        </div>

        <div class="content-right">
            <h2>VISÃO</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi nisi, unde praesentium. Perspiciatis dolores sunt rem eum deserunt quaerat ullam, nemo, vitae facere corporis! Animi.</p>
            <h2>MISSÃO</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi nisi, unde praesentium. Perspiciatis dolores sunt rem eum deserunt quaerat ullam, nemo, vitae facere corporis! Animi.</p>
            <h2>VALORES</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi nisi, unde praesentium. Perspiciatis dolores sunt rem eum deserunt quaerat ullam, nemo, vitae facere corporis! Animi.</p>
        </div>
    </div>
</div>