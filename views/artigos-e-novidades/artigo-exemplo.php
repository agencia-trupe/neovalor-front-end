<div class="content artigos-novidades">
    <div class="center">
        <div class="content-left">
            <div class="categorias">
                <a href="#">TODOS</a>
                <a href="<?=$url?>artigos-e-novidades/categoria-exemplo" class="active">CATEGORIA PARA FILTRAR</a>
                <a href="<?=$url?>artigos-e-novidades/categoria-exemplo">CATEGORIA PARA FILTRAR</a>
                <a href="<?=$url?>artigos-e-novidades/categoria-exemplo">CATEGORIA</a>
            </div>
        </div>

        <div class="content-main artigo">
            <h1>ARTIGOS & NOVIDADES</h1>

            <p class="data">02 FEV 2016 &middot; Nome da Categoria</p>
            <h2>TÍTULO COMPLETO DO ARTIGO EM CAIXA ALTA</h2>

            <p><img src="<?=$url?>assets/img/artigo.jpg" alt=""></p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel quasi tenetur ut itaque omnis vero necessitatibus neque repellat earum repudiandae! Iusto, impedit fugit cumque vitae, unde nulla dolor error id voluptatibus, saepe incidunt soluta. Ad perspiciatis excepturi molestias tenetur quam, aliquam nobis natus error pariatur saepe molestiae, eligendi adipisci quae nulla itaque! Minus ea perspiciatis voluptates at, suscipit impedit quis voluptas hic architecto dolorum. Ullam doloremque nesciunt recusandae quisquam, ad.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita impedit tenetur incidunt, officia, saepe quo ipsam debitis quae ad atque voluptatibus cupiditate beatae minima numquam tempora natus accusamus. Ratione saepe a, debitis iusto molestias fugiat eos ea suscipit numquam odio enim! Minus tenetur voluptas illum eum dolor saepe, voluptatem beatae.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur quia adipisci quaerat temporibus, quam eveniet, cupiditate, nemo odio itaque mollitia quas blanditiis. Repellendus et distinctio eos nesciunt, doloribus magnam ut.</p>

            <div class="video">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/IxumhXTD9o4" frameborder="0" allowfullscreen></iframe>
            </div>

            <div class="galeria">
<?php for($i = 0; $i < 9; $i++) { ?>
                <a href="<?=$url?>assets/img/artigo.jpg" class="fancybox" rel="galeria">
                    <img src="<?=$url?>assets/img/artigo-thumb.jpg" alt="">
                </a>
<?php } ?>
            </div>

            <a href="#" class="voltar">&laquo; voltar para ver mais Artigos & Novidades</a>
        </div>

        <div class="content-right">
            <div class="outros-conteudos">
                <h3>OUTROS CONTEÚDOS RECENTES</h3>
<?php for ($i = 0; $i < 4; $i++) { ?>
                <a href="<?=$url?>artigos-e-novidades/artigo-exemplo" class="artigo-chamada">
                    <p class="data">02 FEV 2016 &middot; Nome da Categoria</p>
                    <h2>Título completo do artigo em caixa alta</h2>
                </a>
<?php } ?>
            </div>

            <div class="newsletter">
                <p>
                    <span>VOCÊ ATUALIZADO</span>
                    CADASTRE-SE PARA RECEBER NOVIDADES
                </p>

                <form action="">
                    <input type="text" name="nome" placeholder="nome" required>
                    <input type="email" name="email" placeholder="e-mail" required>
                    <input type="submit" value="CADASTRAR">
                    <div class="response">Cadastro efetuado com sucesso!</div>
                </form>
            </div>
        </div>
    </div>
</div>