<div class="content artigos-novidades">
    <div class="center">
        <div class="content-left">
            <div class="categorias">
                <a href="#" class="active">TODOS</a>
                <a href="<?=$url?>artigos-e-novidades/categoria-exemplo">CATEGORIA PARA FILTRAR</a>
                <a href="<?=$url?>artigos-e-novidades/categoria-exemplo">CATEGORIA PARA FILTRAR</a>
                <a href="<?=$url?>artigos-e-novidades/categoria-exemplo">CATEGORIA</a>
            </div>
        </div>

        <div class="content-main lista">
            <h1>ARTIGOS & NOVIDADES</h1>

<?php for ($i = 0; $i < 6; $i++) { ?>
            <a href="<?=$url?>artigos-e-novidades/artigo-exemplo" class="artigo-chamada">
                <p class="data">02 FEV 2016 &middot; Nome da Categoria</p>
                <h2>TÍTULO COMPLETO DO ARTIGO EM CAIXA ALTA</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus similique dignissimos hic sequi eaque odio quae molestias, nam asperiores suscipit.</p>
            </a>
<?php } ?>

            <div class="paginacao">
                <span class="active">1</span>
                <a href="#">2</a>
                <a href="#">3</a>
                <a href="#">4</a>
                <a href="#">&raquo;</a>
            </div>
        </div>

        <div class="content-right">
            <img src="<?=$url?>assets/img/img-artigosnovidades.png" alt="">
            <div class="newsletter">
                <p>
                    <span>VOCÊ ATUALIZADO</span>
                    CADASTRE-SE PARA RECEBER NOVIDADES
                </p>

                <form action="">
                    <input type="text" name="nome" placeholder="nome" required>
                    <input type="email" name="email" placeholder="e-mail" required>
                    <input type="submit" value="CADASTRAR">
                    <div class="response">Cadastro efetuado com sucesso!</div>
                </form>
            </div>
        </div>
    </div>
</div>