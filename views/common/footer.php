
<footer>
    <div class="informacoes">
        <div class="center">
            <div class="col col-links">
                <a href="#" class="link-main">A NEOVALOR</a>
                <a href="#">institucional</a>
                <a href="#">consultores</a>
            </div>

            <div class="col col-links">
                <a href="#" class="link-main">TREINAMENTOS</a>
                <a href="#">áreas de atuação</a>
                <a href="#">nossos treinamentos</a>
                <a href="#">programas customizados</a>
                <a href="#">agenda de cursos</a>
            </div>

            <div class="col col-links">
                <a href="#" class="link-main">CONSULTORIA</a>
                <a href="#" class="link-main">FAST TRACK</a>
                <a href="#" class="link-main">ARTIGOS & NOVIDADES</a>
                <a href="#" class="link-main">MARKETING DIGITAL</a>
            </div>

            <div class="col col-contato">
                <a href="#" class="link-main">CONTATO</a>
                <div class="contato-info">
                    <img src="<?=$url?>assets/img/marca-neovalor-footer.png" alt="">
                    <div class="social">
                        <a href="#" class="facebook"></a>
                        <a href="#" class="linkedin"></a>
                        <a href="#" class="email"></a>
                    </div>
                    <p>NEOVALOR &middot; São Paulo &middot; <span>+55 11 5678&middot;4321</span></p>
                    <p>Rua do Endereço Completo, 123 &middot; cj 45 &middot; Vila do Bairro<br>01234-567 &middot; São Paulo &middot; SP</p>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright">
        <div class="center">
            <p>
                © 2016 NEOVALOR · todos os direitos reservados. |
                <span>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa.</a>
                </span>
            </p>
        </div>
    </div>
</footer>
