<div class="header-multiply" <?php if(! isHome($menu)) echo 'style="display:none"' ?>></div>
<header <?php if(isHome($menu)) echo 'class="home"' ?>>
    <div class="center">
        <a href="<?=$url?>" class="logo">neovalor</a>

        <nav class="nav-main">
            <a href="<?=$url?>treinamentos"<?php if($menu == 'treinamentos') echo 'class="active"' ?>>TREINAMENTOS</a>
            <a href="<?=$url?>consultoria"<?php if($menu == 'consultoria') echo 'class="active"' ?>>CONSULTORIA</a>
            <a href="<?=$url?>fast-track"<?php if($menu == 'fast-track') echo 'class="active"' ?>>FAST TRACK</a>
            <a href="<?=$url?>marketing-digital"<?php if($menu == 'marketing-digital') echo 'class="active"' ?>>MARKETING DIGITAL</a>
        </nav>

        <nav class="nav-list">
            <a href="<?=$url?>a-neovalor"<?php if($menu == 'a-neovalor') echo 'class="active"' ?>>A NEOVALOR</a>
            <a href="<?=$url?>artigos-e-novidades"<?php if($menu == 'artigos-e-novidades') echo 'class="active"' ?>>ARTIGOS & NOVIDADES</a>
            <a href="<?=$url?>contato"<?php if($menu == 'contato') echo 'class="active"' ?>>CONTATO</a>
            <a href="#" class="area-do-cliente">ÁREA DO CLIENTE</a>
        </nav>

        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </div>

    <div class="nav-mobile">
        <a href="<?=$url?>treinamentos"<?php if($menu == 'treinamentos') echo 'class="active"' ?>>TREINAMENTOS</a>
        <a href="<?=$url?>consultoria"<?php if($menu == 'consultoria') echo 'class="active"' ?>>CONSULTORIA</a>
        <a href="<?=$url?>fast-track"<?php if($menu == 'fast-track') echo 'class="active"' ?>>FAST TRACK</a>
        <a href="<?=$url?>marketing-digital"<?php if($menu == 'marketing-digital') echo 'class="active"' ?>>MARKETING DIGITAL</a>
        <a href="<?=$url?>a-neovalor"<?php if($menu == 'a-neovalor') echo 'class="active"' ?>>A NEOVALOR</a>
        <a href="<?=$url?>artigos-e-novidades"<?php if($menu == 'artigos-e-novidades') echo 'class="active"' ?>>ARTIGOS & NOVIDADES</a>
        <a href="<?=$url?>contato"<?php if($menu == 'contato') echo 'class="active"' ?>>CONTATO</a>
        <a href="#" class="area-do-cliente">ÁREA DO CLIENTE</a>
    </div>
</header>

