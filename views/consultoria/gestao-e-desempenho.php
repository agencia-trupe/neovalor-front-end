<div class="content consultoria">
    <div class="center">
        <div class="content-left">
            <img src="<?=$url?>assets/img/img-consultoria.png" alt="">
            <div class="categorias">
                <a href="<?=$url?>consultoria">CONSULTORIA</a>
                <a href="<?=$url?>consultoria/gestao-e-desempenho" class="active">GESTÃO & DESEMPENHO</a>
                <a href="#">VENDAS & MARKETING</a>
                <a href="#">PRODUÇÃO & LOGÍSTICA</a>
            </div>
        </div>

        <div class="content-main">
            <h1>CONSULTORIA - GESTÃO E DESEMPENHO</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, expedita. Libero at cupiditate natus commodi minima, sequi quas deleniti maxime in, excepturi nulla dignissimos harum accusamus, quia omnis necessitatibus dolore.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam officiis dignissimos porro, nostrum velit autem libero animi soluta illo at magni nam assumenda commodi reprehenderit eos obcaecati ducimus unde odio consequuntur rerum, consectetur cumque ipsa cum! Omnis maxime fugit, incidunt necessitatibus porro consequuntur repellat praesentium iure voluptatem laborum ex, temporibus!</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda in accusantium natus quam quos quo, obcaecati repellat molestias odit, deserunt?</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At perferendis dolor, minima dolores fugiat alias ex tenetur aliquam officiis est illum accusantium sequi iste molestiae culpa, laudantium eum aut! Deleniti ipsum saepe, veniam sapiente cupiditate minima facilis, quisquam impedit nostrum ducimus sequi maxime dolore. Accusantium totam velit soluta minus quasi. Sit id itaque at maiores quae nemo, temporibus totam soluta.</p>
        </div>

        <div class="content-right">
            <a href="<?=$url?>contato" class="conversa">
                <span>CONVIDAMOS VOCÊ<br> PARA UMA CONVERSA</span>
                <img src="<?=$url?>assets/img/img-convidamosvoce-neovalor.png" alt="">
            </a>
        </div>
    </div>
</div>