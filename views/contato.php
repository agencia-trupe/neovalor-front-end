<div class="content contato">
    <div class="center">
        <div class="content-left">
            <p class="tel">11 5566-7788</p>
            <p>segundas a sextas das 9h às 18h</p>
            <p>sábados das 9h às 13h</p>
        </div>

        <div class="content-main">
            <h1>CONTATO</h1>
            <h1 class="fale-conosco">FALE CONOSCO</h1>
            <form action="">
                <label>
                    <span>nome</span>
                    <input type="text" name="nome" required>
                </label>
                <label>
                    <span>e-mail</span>
                    <input type="email" name="email" required>
                </label>
                <label>
                    <span>telefone</span>
                    <input type="text" name="telefone">
                </label>
                <label>
                    <span>mensagem</span>
                    <textarea name="mensagem"></textarea>
                </label>
                <input type="submit" value="ENVIAR">
            </form>
        </div>

        <div class="content-right">
            <p>Rua do Endereço completo, 123 &middot; cj 45</p>
            <p>Vila do Bairro</p>
            <p>01234-567 &middot; São Paulo &middot; SP</p>

            <div class="mapa">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.3872932557965!2d-46.666232484503446!3d-23.55452988468624!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59c8da0aa315%3A0xd59f9431f2c9776a!2sAv.+Paulista+-+Bela+Vista%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1462999872916" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>