<div class="content fast-track">
    <div class="center">
        <div class="content-left">
            <img src="<?=$url?>assets/img/img-fasttrack.png" alt="">
        </div>

        <div class="content-main">
            <h1>FAST TRACK</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam labore voluptatibus natus placeat est, earum velit quia voluptates dolorem, eum ab aliquid quis nobis? Dolorem exercitationem illo ipsa, sed! Assumenda.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis perferendis tempore sunt, dicta blanditiis libero cum porro dolores consectetur excepturi nemo labore similique delectus ex! Harum soluta, debitis earum officiis ullam illum eligendi accusantium. Earum ratione iste maxime accusamus odit praesentium omnis tempore iusto, ut molestias explicabo similique, quam eaque.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint incidunt delectus ut neque, quaerat quia, vitae. Culpa, quae quidem alias?</p>

            <div class="accordion">
                <p>Atuação:</p>

                <a href="#" class="accordion-handle">ABERTURA DE EMPRESA</a>
                <div class="accordion-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus quas eum dicta repudiandae eligendi soluta doloribus vel provident debitis labore?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, doloribus.</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, expedita.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, molestiae.</li>
                    </ul>
                </div>

                <a href="#" class="accordion-handle">REAVALIAÇÃO ESTRATÉGICA</a>
                <div class="accordion-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus quas eum dicta repudiandae eligendi soluta doloribus vel provident debitis labore?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, doloribus.</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, expedita.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, molestiae.</li>
                    </ul>
                </div>

                <a href="#" class="accordion-handle">LOREM IPSUM DOLOR</a>
                <div class="accordion-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus quas eum dicta repudiandae eligendi soluta doloribus vel provident debitis labore?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, doloribus.</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, expedita.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, molestiae.</li>
                    </ul>
                </div>

                <a href="#" class="accordion-handle">SIT AMET CONSECTETUR</a>
                <div class="accordion-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus quas eum dicta repudiandae eligendi soluta doloribus vel provident debitis labore?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, doloribus.</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, expedita.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, molestiae.</li>
                    </ul>
                </div>
            </div>

            <form action="#">
                <h3>QUAL A SUA NECESSIDADE? CONTATE-NOS</h3>

                <div class="form-wrapper">
                    <input type="text" name="nome" placeholder="nome" required>
                    <input type="email" name="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" placeholder="telefone">
                    <textarea name="mensagem" placeholder="mensagem" required></textarea>
                    <input type="submit" value="ENVIAR">
                </div>
            </form>
        </div>

        <div class="content-right">
            <a href="<?=$url?>contato" class="conversa">
                <span>CONVIDAMOS VOCÊ<br> PARA UMA CONVERSA</span>
                <img src="<?=$url?>assets/img/img-convidamosvoce-neovalor.png" alt="">
            </a>
        </div>
    </div>
</div>