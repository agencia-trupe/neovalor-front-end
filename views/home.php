<div class="content content-home">
    <div class="banners">
        <div class="slide" style="background-image: url('<?=$url?>assets/img/banner-home1.png');">
            <div class="center">
                <div class="slide-texto">
                    <h2>CONSULTORIA</h2>
                    <p>Liderar mudanças para um desempenho melhor. Juntos.</p>
                </div>
            </div>
        </div>

        <div class="slide" style="background-image: url('<?=$url?>assets/img/banner-home1.png');">
            <div class="center">
                <div class="slide-texto">
                    <h2>LOREM IPSUM</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>

        <div class="center">
            <div class="cycle-pager"></div>
        </div>
    </div>

    <div class="apresentacao">
        <div class="center">
            <h3>A NEOVALOR É UMA BOUTIQUE DE SERVIÇOS EMPRESARIAIS ESPECIALIZADA EM TREINAMENTO E CONSULTORIA: DESENVOLVEMOS SOLUÇÕES PARA SEU MELHOR DESEMPENHO</h3>
            <p>Somos consultores com experiência executiva cujo objetivo é atuar junto com os Clientes no entendimento de suas necessidades, oferecer soluções, compartilhar conhecimento e, juntos, Liderar Mudanças para um Desempenho Melhor</p>
        </div>
    </div>

    <div class="center chamadas-1">
        <div class="agendamentos">
            <h3>AGENDE-SE PARA OS PRÓXIMOS TREINAMENTOS</h3>

            <a href="#" class="agendamento">
                <div class="data">
                    <span class="dia">28</span>
                    <span class="mes">MAR</span>
                    <span class="ano">2016</span>
                </div>

                <div class="informacoes">
                    <span class="local">SÃO PAULO - SP</span>
                    <p>Formações de Líderes para empresas do segmento financeiro brasileiro título com duas linhas se necessário</p>
                </div>

                <div class="saiba-mais">SAIBA MAIS</div>
            </a>
            <a href="#" class="agendamento">
                <div class="data">
                    <span class="dia">28</span>
                    <span class="mes">MAR</span>
                    <span class="ano">2016</span>
                </div>

                <div class="informacoes">
                    <span class="local">SÃO PAULO - SP</span>
                    <p>Formações de Líderes para empresas</p>
                </div>

                <div class="saiba-mais">SAIBA MAIS</div>
            </a>
            <a href="#" class="agendamento">
                <div class="data">
                    <span class="dia">28</span>
                    <span class="mes">MAR</span>
                    <span class="ano">2016</span>
                </div>

                <div class="informacoes">
                    <span class="local">SÃO PAULO - SP</span>
                    <p>Formações de Líderes para empresas do segmento financeiro brasileiro título com duas linhas se necessário</p>
                </div>

                <div class="saiba-mais">SAIBA MAIS</div>
            </a>

            <a href="#" class="agenda-completa">AGENDA COMPLETA <span></span></a>
        </div>

        <div class="treinamentos">
            <a href="#">
                <span>TREINAMENTOS IN COMPANY</span>
                DESENVOLVEMOS PROGRAMAS ATENDENDO SUAS NECESSIDADES
            </a>
            <a href="#">
                <span>TREINAMENTOS IN COMPANY</span>
                DESENVOLVEMOS PROGRAMAS ATENDENDO SUAS NECESSIDADES
            </a>
            <a href="#">
                <span>TREINAMENTOS IN COMPANY</span>
                DESENVOLVEMOS PROGRAMAS ATENDENDO SUAS NECESSIDADES
            </a>
        </div>

        <div class="newsletter">
            <p>
                <span>VOCÊ ATUALIZADO</span>
                CADASTRE-SE PARA RECEBER NOVIDADES
            </p>

            <form action="">
                <input type="text" name="nome" placeholder="nome" required>
                <input type="email" name="email" placeholder="e-mail" required>
                <input type="submit" value="CADASTRAR">
                <div class="response">Cadastro efetuado com sucesso!</div>
            </form>
        </div>
    </div>

    <div class="depoimentos">
        <div class="slide">
            <div class="center">
                <div class="pessoa">
                    <p>
                        Nome da pessoa
                        <span>especialista em negócios</span>
                    </p>
                    <div class="imagem">
                        <img src="<?=$url?>assets/img/img-consultor.png" alt="">
                    </div>
                </div>

                <div class="texto">
                    <h3>PALAVRA DO CONSULTOR</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, dolores, voluptates similique consequuntur harum incidunt.</p>
                </div>
            </div>
        </div>
        <div class="slide">
            <div class="center">
                <div class="pessoa">
                    <p>
                        Nome da pessoa
                        <span>especialista em negócios</span>
                    </p>
                    <div class="imagem">
                        <img src="<?=$url?>assets/img/img-consultor.png" alt="">
                    </div>
                </div>

                <div class="texto">
                    <h3>PALAVRA DO CONSULTOR</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, dolores, voluptates similique consequuntur harum incidunt.</p>
                </div>
            </div>
        </div>
        <div class="slide">
            <div class="center">
                <div class="pessoa">
                    <p>
                        Nome da pessoa
                        <span>especialista em negócios</span>
                    </p>
                    <div class="imagem">
                        <img src="<?=$url?>assets/img/img-consultor.png" alt="">
                    </div>
                </div>

                <div class="texto">
                    <h3>PALAVRA DO CONSULTOR</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, dolores, voluptates similique consequuntur harum incidunt.</p>
                </div>
            </div>
        </div>

        <div class="center">
            <div class="cycle-pager"></div>
        </div>
    </div>

    <div class="chamadas-2">
        <div class="center">
            <a href="#" class="area-do-cliente">
                <div class="wrapper">
                    <div class="botao">ÁREA DO CLIENTE</div>
                    <p>ESPAÇO EXCLUSIVO para contatos, conteúdos, andamento do Projeto e outras informações</p>
                </div>
            </a>

            <div class="artigos-novidades">
                <h3>ARTIGOS & NOVIDADES</h3>
                <p class="data">29 JANEIRO 2016</p>
                <p>Título do artigo recente publicado na área de conteúdo do site.</p>
                <a href="#">SAIBA MAIS &raquo;</a>
            </div>

            <div class="chamadas-imagens">
                <a href="#" class="chamada-treinamentos">
                    <div class="wrapper">
                        TREINAMENTOS
                    </div>
                </a>
                <a href="#" class="chamada-consultorias">
                    <div class="wrapper">
                        CONSULTORIAS
                    </div>
                </a>
                <a href="#" class="chamada-fast-track">
                    <div class="wrapper">
                        FAST TRACK
                    </div>
                </a>
                <a href="#" class="chamada-marketing-digital">
                    <div class="wrapper">
                        MARKETING DIGITAL
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>