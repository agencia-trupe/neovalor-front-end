<div class="content marketing-digital">
    <div class="center">
        <div class="content-left">
            <img src="<?=$url?>assets/img/img-marketingdigital.png" alt="">
        </div>

        <div class="content-main">
            <h1>MARKETING DIGITAL</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim, reprehenderit, sit! Culpa, sed quae aliquam.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, ducimus dignissimos illo id eligendi magnam sint a esse accusamus quae soluta perferendis temporibus voluptatum necessitatibus assumenda hic, et iusto numquam officia nam eaque ad odio. Accusamus eligendi, quam qui assumenda architecto error eius cumque, dicta!</p>

            <div class="accordion">
                <p>Atuamos:</p>

                <a href="#" class="accordion-handle">PREPARAR A ESTRATÉGIA</a>
                <div class="accordion-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus quas eum dicta repudiandae eligendi soluta doloribus vel provident debitis labore?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, doloribus.</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, expedita.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, molestiae.</li>
                    </ul>
                </div>

                <a href="#" class="accordion-handle">DESENVOLVER O SITE</a>
                <div class="accordion-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus quas eum dicta repudiandae eligendi soluta doloribus vel provident debitis labore?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, doloribus.</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, expedita.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, molestiae.</li>
                    </ul>
                </div>

                <a href="#" class="accordion-handle">ELABORAR EMAIL MARKETING</a>
                <div class="accordion-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus quas eum dicta repudiandae eligendi soluta doloribus vel provident debitis labore?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, doloribus.</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, expedita.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, molestiae.</li>
                    </ul>
                </div>

                <a href="#" class="accordion-handle">BRANDING</a>
                <div class="accordion-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus quas eum dicta repudiandae eligendi soluta doloribus vel provident debitis labore?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, doloribus.</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, expedita.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, molestiae.</li>
                    </ul>
                </div>
            </div>

            <p>Parceiros</p>
            <ul>
                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, eius!</li>
                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, animi.</li>
            </ul>
        </div>

        <div class="content-right">
            <a href="<?=$url?>contato" class="conversa">
                <span>CONVIDAMOS VOCÊ<br> PARA UMA CONVERSA</span>
                <img src="<?=$url?>assets/img/img-convidamosvoce-neovalor.png" alt="">
            </a>
        </div>
    </div>
</div>