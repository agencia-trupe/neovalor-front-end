<div class="content treinamentos">
    <div class="center">
        <div class="content-left">
            <div class="categorias">
                <a href="<?=$url?>treinamentos">TEMAS & CURSOS</a>
                <a href="<?=$url?>treinamentos/agenda-completa" class="active">AGENDA COMPLETA</a>
                <a href="<?=$url?>treinamentos/treinamento-in-company">TREINAMENTO IN COMPANY</a>
                <a href="<?=$url?>treinamentos/consultores">CONSULTORES</a>
            </div>
        </div>

        <div class="content-main agenda-completa">
            <h1>AGENDA COMPLETA</h1>

<?php for ($i = 0; $i < 10; $i++) { ?>
            <a href="<?=$url?>treinamentos/curso">
                <div class="data">
                    <span class="dia">28</span>
                    <span class="mes">MAR</span>
                    <span class="ano">2016</span>
                </div>

                <div class="informacoes">
                    <span class="local">SÃO PAULO - SP</span>
                    <p>Formações de Líderes para empresas do segmento financeiro brasileiro título com duas linhas se necessário</p>
                </div>

                <div class="saiba-mais">INSCREVA-SE</div>
            </a>
<?php } ?>
        </div>

        <div class="content-right">
            <a href="#" class="interesse">
                <h4>O curso que você quer fazer não tem a data ideal?</h4>
                <p>Declare seu interesse e seja avisado quando novas turmas se formarem</p>
                <span class="seta"></span>
            </a>

            <div class="newsletter">
                <p>
                    <span>VOCÊ ATUALIZADO</span>
                    CADASTRE-SE PARA RECEBER NOVIDADES
                </p>

                <form action="">
                    <input type="text" name="nome" placeholder="nome" required>
                    <input type="email" name="email" placeholder="e-mail" required>
                    <input type="submit" value="CADASTRAR">
                    <div class="response">Cadastro efetuado com sucesso!</div>
                </form>
            </div>
        </div>
    </div>
</div>