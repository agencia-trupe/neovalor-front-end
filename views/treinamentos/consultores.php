<div class="content treinamentos">
    <div class="center">
        <div class="content-left">
            <div class="categorias">
                <a href="<?=$url?>treinamentos">TEMAS & CURSOS</a>
                <a href="<?=$url?>treinamentos/agenda-completa">AGENDA COMPLETA</a>
                <a href="<?=$url?>treinamentos/treinamento-in-company">TREINAMENTO IN COMPANY</a>
                <a href="<?=$url?>treinamentos/consultores" class="active">CONSULTORES</a>
            </div>
        </div>

        <div class="content-main consultores">
            <h1>CONSULTORES</h1>

<?php for ($i = 0; $i < 10; $i++) { ?>
            <div class="consultor">
                <div class="imagem">
                    <img src="<?=$url?>assets/img/img-consultor.png" alt="">
                </div>

                <div class="texto">
                    <h3>Nome do Consultor</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, deleniti, necessitatibus. Repudiandae, soluta placeat quaerat dolor corporis, esse distinctio, minima repellendus necessitatibus ab rerum adipisci architecto accusamus obcaecati dignissimos voluptates commodi. Aliquam quidem velit, assumenda tenetur tempora autem repellat qui quos. Sit modi id asperiores earum beatae dolorum ratione hic.</p>

                    <p class="mais-informacoes">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, amet illo nihil non. Dolores maxime eos accusantium expedita veniam autem nam, ratione labore magni officia itaque asperiores. Officiis consectetur minus tempora dolorem odit cumque atque voluptas expedita repudiandae sit quo libero id optio consequuntur saepe vel labore, voluptatum voluptatibus reprehenderit laborum magnam est blanditiis quibusdam quasi. Dolores optio blanditiis quibusdam.</p>
                    <a href="#" class="mais-informacoes-handle"></a>
                </div>

                <div class="palavra">
                    <h5>PALAVRA DO CONSULTOR</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias eos libero voluptatem possimus rerum tenetur quidem, vero.</p>
                </div>
            </div>
<?php } ?>
        </div>
    </div>
</div>