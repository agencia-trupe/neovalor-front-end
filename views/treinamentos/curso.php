<div class="content treinamentos">
    <div class="center">
        <div class="content-left">
            <div class="categorias">
                <a href="<?=$url?>treinamentos" class="active">TEMAS & CURSOS</a>
                <a href="<?=$url?>treinamentos/agenda-completa">AGENDA COMPLETA</a>
                <a href="<?=$url?>treinamentos/treinamento-in-company">TREINAMENTO IN COMPANY</a>
                <a href="<?=$url?>treinamentos/consultores">CONSULTORES</a>
            </div>
        </div>

        <div class="content-main curso">
            <h1>TEMAS & CURSOS</h1>

            <h3>GESTÃO & LIDERANÇA</h3>
            <h2 class="curso-titulo">FORMAÇÕES DE LÍDERES PARA EMPRESAS DO SEGMENTO FINANCEIRO BRASILEIRO TÍTULO COM DUAS LINHAS SE NECESSÁRIO</h2>

            <div class="curso-descricao">
                <h4>SOBRE O CURSO</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque maxime ipsa ratione, aspernatur rerum. Quis porro, impedit? Necessitatibus tempore, sit quae illo culpa ut, hic labore nam rerum non accusamus.</p>

                <h4>A QUEM SE DESTINA</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque culpa, molestiae eligendi voluptatibus possimus assumenda nemo distinctio voluptates totam nisi quaerat. Doloremque facere, sequi illo necessitatibus natus tenetur recusandae itaque optio magnam quo, fugiat alias molestiae velit praesentium perspiciatis impedit.</p>

                <h4>OBJETIVO</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, repudiandae tenetur in odit! Dignissimos accusantium error provident consequatur, nemo ab.</p>

                <h4>CARGA HORÁRIA</h4>
                <p>16 horas</p>

                <h4>PROGRAMA DO CURSO</h4>
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat, excepturi?</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, magnam?</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, deleniti.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, qui.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, quia.</li>
                </ul>
            </div>

            <div class="curso-chamadas">
<?php for ($i = 0; $i < 2; $i++) { ?>
                <div class="chamada">
                    <div class="left">
                        <p class="data">
                            28 MARÇO 2016 A 30 ABRIL 2016
                            <span>segundas e quartas - das 19h às 22h</span>
                        </p>

                        <p class="endereco">
                            São Paulo - SP<br>
                            Centro de Convenções Rebouças<br>
                            Av. Rebouças, 2304 - Jardim Paulistano
                        </p>

                        <a href="#" class="ver-mapa">VER MAPA <span></span></a>
                    </div>

                    <div class="right">
                        <p class="titulo">INVESTIMENTO</p>

                        <p>
                            Inscrição Antecipada com desconto até 20/03<br>
                            <strong>R$ 2.250,00</strong> - Inscrição Individual<br>
                            <strong>R$ 2.200,00</strong> - Inscrição em Grupo*<br>
                        </p>

                        <p>
                            Inscrição a partir de 21/03<br>
                            <strong>R$ 2.500,00</strong> - Inscrição Individual<br>
                            <strong>R$ 2.300,00</strong> - Inscrição em Grupo*<br>
                        </p>

                        <p class="legenda">(*) mínimo de 3 participantes, valor por participante</p>
                    </div>

                    <a href="<?=$url?>treinamentos/inscricao-1" class="botao-inscricao">FAZER MINHA INSCRIÇÃO</a>
                </div>
<?php } ?>
            </div>

            <div class="curso-interesse">
                <h4>REGISTRE SEU INTERESSE PARA NOVAS DATAS</h4>
                <p>Caso tenha interesse por este Treinamento em outras datas ou locais, por favor, envie-nos suas preferências.</p>

                <form action="">
                    <input type="text" name="nome" placeholder="nome" required>
                    <input type="email" name="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" placeholder="telefone">
                    <div class="row">
                        <span>Sugestão de datas ou mês:</span>
                        <input type="text" name="data" placeholder="data ou mês" required>
                    </div>
                    <div class="row">
                        <span>Em São Paulo ou outra localidade:</span>
                        <input type="text" name="cidade" placeholder="cidade" required>
                    </div>
                    <div class="row">
                        <span>Informe sua preferência por período:</span>
                        <div class="checkboxes">
                            <label>
                                <input type="checkbox" name="periodo" value="semana_integral">
                                durante a semana período Integral
                            </label>
                            <label>
                                <input type="checkbox" name="periodo" value="semana_manha">
                                durante a semana período da Manhã
                            </label>
                            <label>
                                <input type="checkbox" name="periodo" value="semana_tarde">
                                durante a semana período da Tarde
                            </label>
                            <label>
                                <input type="checkbox" name="periodo" value="semana_noite">
                                durante a semana período da Noite
                            </label>
                            <label>
                                <input type="checkbox" name="periodo" value="sabados_manha">
                                aos Sábados: período da Manhã
                            </label>
                            <label>
                                <input type="checkbox" name="periodo" value="sabados_integral">
                                aos Sábados: período Integral
                            </label>
                        </div>
                    </div>
                    <input type="submit" value="ENVIAR">
                </form>
            </div>
        </div>

        <div class="content-right">
            <div class="consultor">
                <div class="imagem">
                    <img src="<?=$url?>assets/img/img-consultor.png" alt="">
                </div>

                <h3>
                    <span>CONSULTOR </span>
                    Nome do Consultor
                </h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et similique distinctio obcaecati hic odit natus sapiente ipsa necessitatibus quis eveniet autem velit, modi nisi magnam amet, quam fugit facere quibusdam ab! Cumque excepturi modi possimus necessitatibus deserunt corrupti voluptatem reprehenderit beatae! Excepturi, debitis porro voluptas.</p>
            </div>

            <div class="outros-temas">
                <h3>SAIBA MAIS SOBRE OUTROS TEMAS E CURSOS</h3>
                <a href="#">&raquo; PRODUÇÃO</a>
                <a href="#">&raquo; GESTÃO & LIDERANÇA</a>
                <a href="#">&raquo; FINCANCEIRO & ADMINISTRATIVO</a>
                <a href="#">&raquo; VENDAS & MARKETING</a>
            </div>
        </div>
    </div>
</div>