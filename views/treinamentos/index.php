<div class="content treinamentos">
    <div class="center">
        <div class="content-left">
            <div class="categorias">
                <a href="<?=$url?>treinamentos" class="active">TEMAS & CURSOS</a>
                <a href="<?=$url?>treinamentos/agenda-completa">AGENDA COMPLETA</a>
                <a href="<?=$url?>treinamentos/treinamento-in-company">TREINAMENTO IN COMPANY</a>
                <a href="<?=$url?>treinamentos/consultores">CONSULTORES</a>
            </div>
        </div>

        <div class="content-main temas-e-cursos">
            <h1>TREINAMENTOS: TEMAS & CURSOS</h1>
            <h2>Conheça todos os nossos treinamentos:</h2>

<?php for ($i = 0; $i < 4; $i++) { ?>
            <h3>LOREM IPSUM DOLOR SIT</h3>
    <?php for ($j = 0; $j < 3; $j++) { ?>
                <a href="<?=$url?>treinamentos/curso">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint voluptatem, pariatur nemo iure molestiae accusamus magni!
                    <div class="saiba-mais">SAIBA MAIS</div>
                </a>
    <?php } ?>
<?php } ?>
        </div>

        <div class="content-right">
            <a href="#" class="interesse">
                <h4>O curso que você quer fazer não tem a data ideal?</h4>
                <p>Declare seu interesse e seja avisado quando novas turmas se formarem</p>
                <span class="seta"></span>
            </a>

            <div class="newsletter">
                <p>
                    <span>VOCÊ ATUALIZADO</span>
                    CADASTRE-SE PARA RECEBER NOVIDADES
                </p>

                <form action="">
                    <input type="text" name="nome" placeholder="nome" required>
                    <input type="email" name="email" placeholder="e-mail" required>
                    <input type="submit" value="CADASTRAR">
                    <div class="response">Cadastro efetuado com sucesso!</div>
                </form>
            </div>
        </div>
    </div>
</div>