<div class="content treinamentos">
    <div class="center">
        <div class="content-left">
            <div class="categorias">
                <a href="<?=$url?>treinamentos" class="active">TEMAS & CURSOS</a>
                <a href="<?=$url?>treinamentos/agenda-completa">AGENDA COMPLETA</a>
                <a href="<?=$url?>treinamentos/treinamento-in-company">TREINAMENTO IN COMPANY</a>
                <a href="<?=$url?>treinamentos/consultores">CONSULTORES</a>
            </div>
        </div>

        <div class="content-main inscricao full-width-tablet">
            <h1>INSCRIÇÃO</h1>
            <h2 class="inscricao-subtitulo">Meus treinamentos selecionados:</h2>

            <h3>GESTÃO & LIDERANÇA</h3>
            <h2 class="curso-titulo">
                FORMAÇÕES DE LÍDERES PARA EMPRESAS DO SEGMENTO FINANCEIRO BRASILEIRO TÍTULO COM DUAS LINHAS SE NECESSÁRIO
                <a href="#" class="excluir"></a>
            </h2>

            <div class="chamada-curso">
                <div class="left">
                    <p class="data">
                        28 MARÇO 2016 A 30 ABRIL 2016
                        <span>segundas e quartas - das 19h às 22h</span>
                    </p>

                    <p class="endereco">
                        São Paulo - SP<br>
                        Centro de Convenções Rebouças<br>
                        Av. Rebouças, 2304 - Jardim Paulistano
                    </p>
                </div>

                <div class="right">
                    <p class="titulo">INVESTIMENTO</p>
                    <p class="valor">R$ 2.250,00</p>
                    <p>Inscrição individual antecipada com desconto até 20 de março</p>
                </div>
            </div>

            <a href="#" class="adicionar-outro">ADICIONAR OUTRO TREINAMENTO PARA INSCRIÇÃO &raquo;</a>

            <p class="titulo-login">REALIZAR MINHA INSCRIÇÃO NOS TREINAMENTOS ACIMA</p>
            <a href="<?=$url?>treinamentos/inscricao-2" class="link-login">JÁ SOU CLIENTE</a>
            <a href="<?=$url?>treinamentos/inscricao-2" class="link-login">NOVO CADASTRO</a>
        </div>
    </div>
</div>