<div class="content treinamentos">
    <div class="center">
        <div class="content-left">
            <div class="categorias">
                <a href="<?=$url?>treinamentos" class="active">TEMAS & CURSOS</a>
                <a href="<?=$url?>treinamentos/agenda-completa">AGENDA COMPLETA</a>
                <a href="<?=$url?>treinamentos/treinamento-in-company">TREINAMENTO IN COMPANY</a>
                <a href="<?=$url?>treinamentos/consultores">CONSULTORES</a>
            </div>
        </div>

        <div class="content-main inscricao">
            <h1>INSCRIÇÃO</h1>
            <h2 class="inscricao-subtitulo">Acompanhe os passos para realizar sua inscrição:</h2>

            <div class="inscricao-passos">
                <div class="passo">
                    <span>&middot; 1 &middot;</span>
                    <p>Criar login</p>
                </div>
                <div class="passo active">
                    <span>&middot; 2 &middot;</span>
                    <p>Informar dados pessoais ou de grupos</p>
                </div>
                <div class="passo">
                    <span>&middot; 3 &middot;</span>
                    <p>Informar dados de pagamento</p>
                </div>
                <div class="passo">
                    <span>&middot; 4 &middot;</span>
                    <p>Finalizar inscrição</p>
                </div>
            </div>

            <h1>DADOS PESSOAIS</h1>

            <div class="dados-pessoais-botoes">
                <a href="<?=$url?>treinamentos/inscricao-4">CADASTRAR UM ÚNICO PARTICIPANTE</a>
                <a href="<?=$url?>treinamentos/inscricao-4-grupo">CADASTRAR UM GRUPO DE PARTICIPANTES</a>
            </div>
        </div>

        <div class="content-right">
            <div class="processo">
                <h3>EM PROCESSO DE INSCRIÇÃO PARA OS SEGUINTES TREINAMENTOS:</h3>

                <div class="treinamento">
                    <p class="titulo">FORMAÇÕES DE LÍDERES PARA EMPRESAS DO SEGMENTO FINANCEIRO BRASILEIRO TÍTULO COM DUAS LINHAS SE NECESSÁRIO</p>
                    <p class="descricao">
                        GESTÃO & LIDERANÇA<br>
                        28 MARÇO 2016 A 30 ABRIL 2016<br>
                        São Paulo, SP
                    </p>
                    <p class="valor">R$ 2.000,00</p>
                </div>
                <div class="treinamento">
                    <p class="titulo">FORMAÇÕES DE LÍDERES PARA EMPRESAS DO SEGMENTO FINANCEIRO BRASILEIRO TÍTULO COM DUAS LINHAS SE NECESSÁRIO</p>
                    <p class="descricao">
                        GESTÃO & LIDERANÇA<br>
                        28 MARÇO 2016 A 30 ABRIL 2016<br>
                        São Paulo, SP
                    </p>
                    <p class="valor">R$ 2.000,00</p>
                </div>
            </div>
        </div>
    </div>
</div>