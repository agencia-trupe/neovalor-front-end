<div class="content treinamentos">
    <div class="center">
        <div class="content-left">
            <div class="categorias">
                <a href="<?=$url?>treinamentos" class="active">TEMAS & CURSOS</a>
                <a href="<?=$url?>treinamentos/agenda-completa">AGENDA COMPLETA</a>
                <a href="<?=$url?>treinamentos/treinamento-in-company">TREINAMENTO IN COMPANY</a>
                <a href="<?=$url?>treinamentos/consultores">CONSULTORES</a>
            </div>
        </div>

        <div class="content-main inscricao">
            <h1>INSCRIÇÃO</h1>
            <h2 class="inscricao-subtitulo">Acompanhe os passos para realizar sua inscrição:</h2>

            <div class="inscricao-passos">
                <div class="passo">
                    <span>&middot; 1 &middot;</span>
                    <p>Criar login</p>
                </div>
                <div class="passo active">
                    <span>&middot; 2 &middot;</span>
                    <p>Informar dados pessoais ou de grupos</p>
                </div>
                <div class="passo">
                    <span>&middot; 3 &middot;</span>
                    <p>Informar dados de pagamento</p>
                </div>
                <div class="passo">
                    <span>&middot; 4 &middot;</span>
                    <p>Finalizar inscrição</p>
                </div>
            </div>

            <h1 class="dados-pessoais">
                DADOS PESSOAIS
                <a href="#">sair [X]</a>
            </h1>

            <form action="<?=$url?>treinamentos/inscricao-5" method="POST" class="dados-pessoais-form">
                <label>
                    <span>CPF</span>
                    <input type="text" name="cpf" required>
                </label>
                <label>
                    <span>nome completo</span>
                    <input type="text" name="nome_completo" required>
                </label>
                <label>
                    <span>nome (crachá)</span>
                    <input type="text" name="nome_cracha" required>
                </label>
                <label>
                    <span>telefone para contato</span>
                    <input type="text" name="telefone" required class="half-width">
                </label>
                <label>
                    <span>outro telefone para contato</span>
                    <input type="text" name="telefone_outro" required class="half-width">
                </label>
                <label>
                    <span>empresa</span>
                    <input type="text" name="empresa" required>
                </label>
                <label>
                    <span>cargo</span>
                    <input type="text" name="cargo" required>
                </label>
                <label>
                    <span>website</span>
                    <input type="text" name="website" required>
                </label>
                <label>
                    <span>CEP</span>
                    <input type="text" name="cep" id="cep" required class="half-width" placeholder="digite para simular a busca">
                    <div class="loading">&middot;&middot;&middot;</div>
                </label>
                <div class="campos-cep hidden">
                    <label>
                        <span>endereço</span>
                        <input type="text" name="endereco" id="endereco" required>
                    </label>
                    <label>
                        <span>número</span>
                        <input type="text" name="numero" required class="quarter-width">
                    </label>
                    <label>
                        <span>complemento</span>
                        <input type="text" name="complemento" class="half-width">
                    </label>
                    <label>
                        <span>bairro</span>
                        <input type="text" name="bairro" id="bairro" required>
                    </label>
                    <label>
                        <span>cidade</span>
                        <input type="text" name="cidade" id="cidade" required>
                    </label>
                    <label>
                        <span>UF</span>
                        <input type="text" name="uf" id="uf" required class="quarter-width">
                    </label>
                    <input type="submit" class="prosseguir" value="PROSSEGUIR &raquo; 3: INFORMAR DADOS DE PAGAMENTO">
                </div>
            </form>
        </div>

        <div class="content-right">
            <div class="processo">
                <h3>EM PROCESSO DE INSCRIÇÃO PARA OS SEGUINTES TREINAMENTOS:</h3>

                <div class="treinamento">
                    <p class="titulo">FORMAÇÕES DE LÍDERES PARA EMPRESAS DO SEGMENTO FINANCEIRO BRASILEIRO TÍTULO COM DUAS LINHAS SE NECESSÁRIO</p>
                    <p class="descricao">
                        GESTÃO & LIDERANÇA<br>
                        28 MARÇO 2016 A 30 ABRIL 2016<br>
                        São Paulo, SP
                    </p>
                    <p class="valor">R$ 2.000,00</p>
                </div>
                <div class="treinamento">
                    <p class="titulo">FORMAÇÕES DE LÍDERES PARA EMPRESAS DO SEGMENTO FINANCEIRO BRASILEIRO TÍTULO COM DUAS LINHAS SE NECESSÁRIO</p>
                    <p class="descricao">
                        GESTÃO & LIDERANÇA<br>
                        28 MARÇO 2016 A 30 ABRIL 2016<br>
                        São Paulo, SP
                    </p>
                    <p class="valor">R$ 2.000,00</p>
                </div>
            </div>
        </div>
    </div>
</div>