<div class="content treinamentos">
    <div class="center">
        <div class="content-left">
            <div class="categorias">
                <a href="<?=$url?>treinamentos" class="active">TEMAS & CURSOS</a>
                <a href="<?=$url?>treinamentos/agenda-completa">AGENDA COMPLETA</a>
                <a href="<?=$url?>treinamentos/treinamento-in-company">TREINAMENTO IN COMPANY</a>
                <a href="<?=$url?>treinamentos/consultores">CONSULTORES</a>
            </div>
        </div>

        <div class="content-main inscricao">
            <h1>INSCRIÇÃO</h1>
            <h2 class="inscricao-subtitulo">Acompanhe os passos para realizar sua inscrição:</h2>

            <div class="inscricao-passos">
                <div class="passo">
                    <span>&middot; 1 &middot;</span>
                    <p>Criar login</p>
                </div>
                <div class="passo">
                    <span>&middot; 2 &middot;</span>
                    <p>Informar dados pessoais ou de grupos</p>
                </div>
                <div class="passo active">
                    <span>&middot; 3 &middot;</span>
                    <p>Informar dados de pagamento</p>
                </div>
                <div class="passo">
                    <span>&middot; 4 &middot;</span>
                    <p>Finalizar inscrição</p>
                </div>
            </div>

            <h1>DADOS DE PAGAMENTO</h1>

            <form action="<?=$url?>treinamentos/inscricao-6" method="POST" class="dados-pagamento-form">
                <p class="titulo-form">Dados do responsável pelo pagamento:</p>
                <label class="responsavel-pagamento">
                    <input type="radio" name="responsavel_pagamento" value="mesmo" checked>
                    o mesmo (participante único)
                </label>
                <label class="responsavel-pagamento">
                    <input type="radio" name="responsavel_pagamento" value="pessoa_fisica">
                    pessoa física (outro)
                </label>
                <label class="responsavel-pagamento">
                    <input type="radio" name="responsavel_pagamento" value="pessoa_juridica">
                    pessoa jurídica
                </label>

                <div class="form-hidden form-pessoa-fisica">
                    <label>
                        <span>CPF</span>
                        <input type="text" name="cpf">
                    </label>
                    <label>
                        <span>nome completo</span>
                        <input type="text" name="nome_completo">
                    </label>
                    <label>
                        <span>nome (crachá)</span>
                        <input type="text" name="nome_cracha">
                    </label>
                    <label>
                        <span>telefone para contato</span>
                        <input type="text" name="telefone" class="half-width">
                    </label>
                    <label>
                        <span>outro telefone para contato</span>
                        <input type="text" name="telefone_outro" class="half-width">
                    </label>
                    <label>
                        <span>empresa</span>
                        <input type="text" name="empresa">
                    </label>
                    <label>
                        <span>cargo</span>
                        <input type="text" name="cargo">
                    </label>
                    <label>
                        <span>website</span>
                        <input type="text" name="website">
                    </label>
                    <label>
                        <span>CEP</span>
                        <input type="text" name="cep" id="cep" class="half-width cep-handle" placeholder="digite para simular a busca">
                        <div class="loading">&middot;&middot;&middot;</div>
                    </label>
                    <div class="campos-cep hidden">
                        <label>
                            <span>endereço</span>
                            <input type="text" name="endereco" class="handle-endereco">
                        </label>
                        <label>
                            <span>número</span>
                            <input type="text" name="numero" class="quarter-width">
                        </label>
                        <label>
                            <span>complemento</span>
                            <input type="text" name="complemento" class="half-width">
                        </label>
                        <label>
                            <span>bairro</span>
                            <input type="text" name="bairro" class="handle-bairro">
                        </label>
                        <label>
                            <span>cidade</span>
                            <input type="text" name="cidade" class="handle-cidade">
                        </label>
                        <label>
                            <span>UF</span>
                            <input type="text" name="uf" class="handle-uf" class="quarter-width">
                        </label>
                        <div class="row">
                            <span>endereço de cobrança</span>
                            <div class="radios">
                                <label>
                                    <input type="radio" name="pf_endereco_cobranca" class="endereco-cobranca-radio" value="mesmo" checked>
                                    o mesmo
                                </label>
                                <label>
                                    <input type="radio" name="pf_endereco_cobranca" class="endereco-cobranca-radio" value="novo">
                                    cadastrar novo
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-hidden form-pessoa-juridica">
                    <label>
                        <span>CNPJ</span>
                        <input type="text" name="cnpj">
                    </label>
                    <label>
                        <span>razão social</span>
                        <input type="text" name="razao_social">
                    </label>
                    <label>
                        <span>inscrição estadual</span>
                        <input type="text" name="inscricao_estadual">
                    </label>
                    <label>
                        <span>CEP</span>
                        <input type="text" name="cep" id="cep" class="half-width cep-handle" placeholder="digite para simular a busca">
                        <div class="loading">&middot;&middot;&middot;</div>
                    </label>
                    <div class="campos-cep hidden">
                        <label>
                            <span>endereço empresa</span>
                            <input type="text" name="endereco" class="handle-endereco">
                        </label>
                        <label>
                            <span>número</span>
                            <input type="text" name="numero" class="quarter-width">
                        </label>
                        <label>
                            <span>complemento</span>
                            <input type="text" name="complemento" class="half-width">
                        </label>
                        <label>
                            <span>bairro</span>
                            <input type="text" name="bairro" class="handle-bairro">
                        </label>
                        <label>
                            <span>cidade</span>
                            <input type="text" name="cidade" class="handle-cidade">
                        </label>
                        <label>
                            <span>UF</span>
                            <input type="text" name="uf" class="handle-uf" class="quarter-width">
                        </label>
                        <label>
                            <span>responsável</span>
                            <input type="text" name="responsavel">
                        </label>
                        <label>
                            <span>e-mail do responsável</span>
                            <input type="text" name="responsavel_email">
                        </label>
                        <label>
                            <span>telefone de contato</span>
                            <input type="text" name="telefone">
                        </label>
                        <div class="row">
                            <span>endereço de cobrança</span>
                            <div class="radios">
                                <label>
                                    <input type="radio" name="pj_endereco_cobranca" class="endereco-cobranca-radio" value="mesmo" checked>
                                    o mesmo
                                </label>
                                <label>
                                    <input type="radio" name="pj_endereco_cobranca" class="endereco-cobranca-radio" value="novo">
                                    cadastrar novo
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="endereco-novo-form">
                    <label>
                        <span>CEP</span>
                        <input type="text" name="novo_cep" id="cep" class="half-width cep-handle" placeholder="digite para simular a busca">
                        <div class="loading">&middot;&middot;&middot;</div>
                    </label>
                    <div class="campos-cep hidden">
                        <label>
                            <span>endereço</span>
                            <input type="text" name="novo_endereco" class="handle-endereco">
                        </label>
                        <label>
                            <span>número</span>
                            <input type="text" name="novo_numero" class="quarter-width">
                        </label>
                        <label>
                            <span>complemento</span>
                            <input type="text" name="novo_complemento" class="half-width">
                        </label>
                        <label>
                            <span>bairro</span>
                            <input type="text" name="novo_bairro" class="handle-bairro">
                        </label>
                        <label>
                            <span>cidade</span>
                            <input type="text" name="novo_cidade" class="handle-cidade">
                        </label>
                        <label>
                            <span>UF</span>
                            <input type="text" name="novo_uf" class="handle-uf" class="quarter-width">
                        </label>
                    </div>
                </div>

                <div class="codigo-promocional">
                    <label>
                        <span>Possui código promocional?</span>
                        <input type="text" name="codigo_promocional" placeholder="inserir código promocional" class="half-width">
                    </label>
                    <a href="#">CALCULAR DESCONTO</a>
                </div>

                <p class="pagamento-valor-total">
                    VALOR TOTAL DOS TREINAMENTOS: R$ 5.000,00
                </p>

                <label class="termos-e-condicoes">
                    <input type="checkbox" name="termos_e_condicoes" value="1" required>
                    Li e aceito os <a href="#">TERMOS E CONDIÇÕES COMERCIAIS</a> para a aquisição do(s) treinamento(s) supra citados.
                </label>

                <p class="titulo-form">Informações Adicionais</p>
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus tempora quod omnis cumque facere ab!</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam suscipit accusamus repellendus animi libero delectus reprehenderit repudiandae ipsa veritatis in.</li>
                </ul>

                <input type="submit" class="prosseguir" value="PROSSEGUIR &raquo;">
            </form>
        </div>

        <div class="content-right">
            <div class="processo">
                <h3>EM PROCESSO DE INSCRIÇÃO PARA OS SEGUINTES TREINAMENTOS:</h3>

                <div class="treinamento">
                    <p class="titulo">FORMAÇÕES DE LÍDERES PARA EMPRESAS DO SEGMENTO FINANCEIRO BRASILEIRO TÍTULO COM DUAS LINHAS SE NECESSÁRIO</p>
                    <p class="descricao">
                        GESTÃO & LIDERANÇA<br>
                        28 MARÇO 2016 A 30 ABRIL 2016<br>
                        São Paulo, SP
                    </p>
                    <p class="valor">R$ 2.000,00</p>
                </div>
                <div class="treinamento">
                    <p class="titulo">FORMAÇÕES DE LÍDERES PARA EMPRESAS DO SEGMENTO FINANCEIRO BRASILEIRO TÍTULO COM DUAS LINHAS SE NECESSÁRIO</p>
                    <p class="descricao">
                        GESTÃO & LIDERANÇA<br>
                        28 MARÇO 2016 A 30 ABRIL 2016<br>
                        São Paulo, SP
                    </p>
                    <p class="valor">R$ 2.000,00</p>
                </div>
                <div class="total">
                    <p>VALOR TOTAL</p>
                    <p class="valor">R$ 5.000,00</p>
                </div>
            </div>
        </div>
    </div>
</div>