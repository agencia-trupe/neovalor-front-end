<div class="content treinamentos">
    <div class="center">
        <div class="content-left">
            <div class="categorias">
                <a href="<?=$url?>treinamentos">TEMAS & CURSOS</a>
                <a href="<?=$url?>treinamentos/agenda-completa">AGENDA COMPLETA</a>
                <a href="<?=$url?>treinamentos/treinamento-in-company" class="active">TREINAMENTO IN COMPANY</a>
                <a href="<?=$url?>treinamentos/consultores">CONSULTORES</a>
            </div>
        </div>

        <div class="content-main treinamento-in-company">
            <h1>TREINAMENTOS IN COMPANY</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas expedita doloribus, sint numquam hic blanditiis eos earum dicta nisi? Inventore velit quidem perferendis quis nisi fugit nihil quam, esse itaque voluptatibus odit, ut quas ullam placeat, tempora numquam. Totam officia saepe accusantium doloremque id dolor, expedita quia quaerat cum. Esse.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, officiis, a. Ducimus ipsam quasi repellat, maiores sapiente amet id harum distinctio nesciunt suscipit tenetur soluta fugit animi, deleniti libero quo.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, tenetur.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, unde, dolore? Aut, non doloribus possimus! Voluptatibus possimus labore, aspernatur consectetur aperiam velit nulla quas rerum quo nisi eius id tempora magni cupiditate deserunt quibusdam architecto illo. Deleniti sequi nihil eius.</p>

            <form action="#">
                <h3>SOLICITE MAIS INFORMAÇÕES SOBRE CURSOS NA SUA EMPRESA</h3>

                <div class="form-wrapper">
                    <input type="text" name="nome" placeholder="nome" required>
                    <input type="email" name="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" placeholder="telefone">
                    <textarea name="mensagem" placeholder="mensagem" required></textarea>
                    <input type="submit" value="ENVIAR">
                </div>
            </form>
        </div>

        <div class="content-right">
            <div class="newsletter">
                <p>
                    <span>VOCÊ ATUALIZADO</span>
                    CADASTRE-SE PARA RECEBER NOVIDADES
                </p>

                <form action="">
                    <input type="text" name="nome" placeholder="nome" required>
                    <input type="email" name="email" placeholder="e-mail" required>
                    <input type="submit" value="CADASTRAR">
                    <div class="response">Cadastro efetuado com sucesso!</div>
                </form>
            </div>
        </div>
    </div>
</div>